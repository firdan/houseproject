package com.housing.project.ui;

import com.housing.project.Chromosome;
import com.housing.project.GALogic;
import com.housing.project.House;
import com.housing.project.LandPlot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class OptimalisasiAG extends JPanel {

    private JLabel timerLbl;
    private int hh, mm, ss;
    private JPanel isi, waktu;
    private JScrollPane scroll;
    private File landFile;
    private JButton resultBtn;

    private static JTextArea log;
    private JTextField generationTxtFld;


    public static Chromosome best;
    Thread jam;

    public OptimalisasiAG() {
        log = new JTextArea(28, 28);

        this.setLayout(null);
        this.setBackground(Color.WHITE);
        setTop();
        setProsesbar();
    }

    public void setTop() {
        JLabel judulLbl, jmlgenLbl, parameterLbl;
        JLabel form1Lbl, form2Lbl, form3Lbl;

        JButton generateBtn;

        JPanel top = new JPanel();
        top.setLayout(null);
        top.setBackground(Color.LIGHT_GRAY);
        top.setBounds(0, 0, 700, 200);

        judulLbl = new JLabel("PROSES OPTIMALISASI ALGORITMA GENETIKA");
        judulLbl.setFont(new Font("Moonhouse", Font.BOLD, 20));
        judulLbl.setBounds(20, 20, 900, 50);

        timerLbl = new JLabel();
        timerLbl.setFont(new Font("Calibri", Font.PLAIN, 22));
        timerLbl.setBounds(275, 165, 900, 700);
        timerLbl.setVerticalAlignment(SwingConstants.TOP);

        parameterLbl = new JLabel("Max Generasi");
        parameterLbl.setFont(new Font("Calibri", Font.PLAIN, 22));
        parameterLbl.setBounds(380, 103, 900, 700);
        parameterLbl.setVerticalAlignment(SwingConstants.TOP);

        form1Lbl = new JLabel();
        form1Lbl.setBounds(210, 88, 100, 20);
        form1Lbl.setFont(new Font("Calibri", Font.PLAIN, 22));
        form2Lbl = new JLabel();
        form2Lbl.setBounds(210, 115, 100, 20);
        form2Lbl.setFont(new Font("Calibri", Font.PLAIN, 22));
        form3Lbl = new JLabel();
        form3Lbl.setBounds(210, 142, 100, 20);
        form3Lbl.setFont(new Font("Calibri", Font.PLAIN, 22));

        generationTxtFld = new JTextField("1");
        generationTxtFld.setBounds(540, 105, 100, 20);

        generateBtn = new JButton("Proses Generasi");
        generateBtn.setBounds(420, 150, 120, 30);
        generateBtn.addActionListener(new GenerateAction());

        resultBtn = new JButton("Hasil (Tata Letak)");
        resultBtn.setBounds(Utils.leftMargin(generateBtn,10), 150, 120, 30);
        resultBtn.addActionListener(new ShowResultAction());
        resultBtn.setEnabled(false);

        top.add(judulLbl);
        top.add(timerLbl);
        top.add(form1Lbl);
        top.add(form2Lbl);
        top.add(form3Lbl);
        top.add(generationTxtFld);
        top.add(parameterLbl);
        top.add(generateBtn);
        top.add(resultBtn);
        this.add(top);
    }

    public void setTime() {
        jam = new Thread() {
            public void run() {
                ss = 0;
                mm = 0;
                hh = 0;
                while (true) {
                    ss++;
                    if (ss == 60) {
                        mm++;
                        ss = 0;
                    }
                    if (mm == 60) {
                        hh++;
                        mm = 0;
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    timerLbl.setText(String.format("%02d", hh) + ":" + String.format("%02d", mm) + ":" + String.format("%02d", ss));
                }
            }
        };
    }

    public static void appendLog(String text){
        log.append(text);
    }

    public void setProsesbar() {
        isi = new JPanel();
        isi.setLayout(new BorderLayout());
        isi.setBackground(Color.WHITE);
        isi.setBounds(0, 200, 700, 500);

        waktu = new JPanel();
        waktu.setLayout(new BorderLayout());
        waktu.setBackground(Color.RED);
        waktu.setBounds(0, 200, 700, 200);

        scroll = new JScrollPane(log);
        isi.add(scroll, BorderLayout.NORTH);
        this.add(isi);
    }

    private class GenerateAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            boolean skip = true;
            int populationSize = 8;
            int maxGenerations = 0;
            double crossoverRatio = 0.5;
            double elitsmRatio = 0.1;
            double mutationRatio = 0.1;

            // check input
            try {
                maxGenerations = Integer.parseInt(generationTxtFld.getText());
                skip = false;
            } catch (NumberFormatException e){
                Utils.infoBox("max generasi harus integer!", "Error!");
            }

            if (!skip) {
                landFile = InputForm.getLandFile();
                // run GAlogic
                GALogic.setLand(landFile.getAbsolutePath(), InputForm.getTotalPlot());
                GALogic galogic = new GALogic();

                galogic.run(populationSize, maxGenerations, crossoverRatio, elitsmRatio, mutationRatio);
                best = galogic.getBest();
                resultBtn.setEnabled(true);
            }

        }
    }

    private class ShowResultAction implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            showPreview();
        }
    }

    private void showPreview(){
        JFrame frame = new JFrame("Land Result");
        JPanel landPreview = new JPanel();

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

//        int[][] landArray = best.getGene();
        int[][] landArray = best.getFilledGene();
        int maxCol = landArray[0].length;
        int maxRow = landArray.length;
        Map mapColor = new HashMap();

        landPreview.removeAll();
        landPreview.setLayout(new GridLayout(maxRow, maxCol));
        landPreview.setBackground(Color.white);
        landPreview.setBorder(BorderFactory.createEmptyBorder(1,0,0,0));

        landPreview.setBounds(0,150, maxCol, maxRow);

        // put color on map
        JPanel grid[][] = new JPanel[maxRow][maxCol];
        for (int row = 0; row < maxRow; row++){
            for (int col = 0; col < maxCol; col++){
                int temp = landArray[row][col];
                grid[row][col]=new JPanel();

                temp = temp > 100 ? temp / 1000: temp;

                if (temp == LandPlot.GRASS) {
                    grid[row][col].setBackground(Color.GREEN);
                } else if (temp == LandPlot.ROAD) {
                    grid[row][col].setBackground(Color.LIGHT_GRAY);
                } else if (temp >= 10) {
                    temp = temp/10;
                    if (mapColor.get(temp+"a") == null ){
                        Random rand = new Random();
                        mapColor.put(temp+"a",rand.nextFloat());
                        mapColor.put(temp+"b",rand.nextFloat());
                        mapColor.put(temp+"c",rand.nextFloat());
                    }
                    grid[row][col].setBackground(new Color((float) mapColor.get(temp+"a"), (float) mapColor.get(temp+"b"), (float) mapColor.get(temp+"c")));
                } else{
                    grid[row][col].setBackground(Color.white);
                }
                landPreview.add(grid[row][col]);
            }
        }


        // set border
        for (int row = 1; row < grid.length -1; row++){
            for (int col = 1; col < grid[row].length -1; col++){
                int currentPlot = landArray[row][col];
                int top = landArray[row-1][col];
                int left = landArray[row][col-1];
                int right = landArray[row][col+1];
                int bottom = landArray[row+1][col];

                grid[row][col].setBorder(BorderFactory.createMatteBorder(
                        currentPlot != top? 1:0,
                        currentPlot != left? 1:0,
                        currentPlot != bottom? 1:0,
                        currentPlot != right ? 1:0,
                        Color.black));

            }
        }


        // add legend
        JPanel legendPnl = new JPanel();
        legendPnl.setLayout(null);
        House[] houses = GALogic.getHouses();
        JLabel title = new JLabel( "LEGEND", JLabel.LEFT);
        title.setBounds(10,7, 100, 10);
        legendPnl.add(title);
        legendPnl.setBounds(0,0,100,100);
        int yPos = 10;
        ArrayList<Integer> shown = new ArrayList<>();

        for (int row = 1; row < grid.length -1; row++){
            for (int col = 1; col < grid[row].length -1; col++){
                // standard operation to get value based on position
                int currentPlot = landArray[row][col];
                int top = landArray[row-1][col];
                int left = landArray[row][col-1];
                int right = landArray[row][col+1];
                int bottom = landArray[row+1][col];

                int temp = landArray[row][col];
                temp = temp > 100 ? temp / 1000: temp;

                if (top != currentPlot && left != currentPlot &&
                        LandPlot.GRASS != currentPlot &&
                        LandPlot.ROAD != currentPlot &&
                        temp >= 10 ){

                    temp /= 10;
                    if (!Utils.checkArray(temp, shown)){
                        House tempHouse = null;

                        for (int i =0; i < houses.length; i++){
                            if ( houses[i].commonId() == temp ){
                                tempHouse = houses[i];
                                break;
                            }
                        }

                        shown.add(temp);

                        yPos += 20;
                        Color tempColor = new Color((float) mapColor.get(temp+"a"), (float) mapColor.get(temp+"b"), (float) mapColor.get(temp+"c"));

                        JPanel box = new JPanel();
                        box.setBounds(10,yPos,10,10);
                        box.setBackground(tempColor);

                        JLabel txt = new JLabel( tempHouse.kode +" "+tempHouse.length+ "x"+tempHouse.width , JLabel.LEFT);
                        txt.setBounds(30, yPos, 70, 10);

                        legendPnl.add(box);
                        legendPnl.add(txt);


                    }
                }
            }
        }

        frame.add(legendPnl);


        landPreview.repaint();
        landPreview.revalidate();

        frame.getContentPane().add(landPreview);
        frame.pack();
        frame.setVisible(true);
    }
}