package com.housing.project.ui;

import com.housing.project.GALogic;
import com.housing.project.LandPlot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedList;

public class InputForm {
    private JButton fileBtn;
    private JButton submitBtn;
    private JPanel inputFormView;
    private JLabel fileLbl;
    private JButton previewBtn;
    private JTextField lengthTxt;
    private JTextField widthTxt;
    private JTextField priceTxt;
    private JButton addPlotBtn;
    private JTextField delCodeTxt;
    private JButton delPlotBtn;
    private JTextField codeTxt;
    private JButton viewConfBtn;
    private JTextArea confTxtArea;
    private JPanel landPreview;

    private JFileChooser fc;

    private static File landFile;
    private static int totalPlot;
    private static int pSize;
    private JPanel rootContainer;

    public InputForm(){

        fileBtn.addActionListener(new AddFileClicked());
        submitBtn.addActionListener(new SubmitClicked());
        previewBtn.addActionListener(new PreviewClicked());
        addPlotBtn.addActionListener(new AddHouseClicked());
        viewConfBtn.addActionListener(new ConfPlotClicked());
        delPlotBtn.addActionListener(new DelHouseClicked());

        previewBtn.setEnabled(false);

        rootContainer = new JPanel();
        landPreview = new JPanel();

        rootContainer.add(inputFormView);

    }

    public JPanel getRootContainer() {
        return rootContainer;
    }

    public static File getLandFile(){
        return landFile;
    }

    public static int getTotalPlot(){
        return totalPlot;
    }

    private class AddFileClicked implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (fc == null){
                fc = new JFileChooser();

                fc.addChoosableFileFilter(new ExcelFilter());
                fc.setAcceptAllFileFilterUsed(false);
            }

            int returnVal = fc.showDialog(inputFormView, "Pilih excel");

            if(returnVal == JFileChooser.APPROVE_OPTION){
                landFile = fc.getSelectedFile();
                System.out.println(landFile.getName());
                fileLbl.setText(landFile.getName());
            }
            fc.setSelectedFile(null);
        }
    }

    private class SubmitClicked implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                totalPlot = 5;
                pSize = 8;
                if (fc == null){
                    Utils.infoBox("Anda Belum menentukan file untuk lahan", "Error!");
                } else{
                    previewBtn.setEnabled(true);
                }
            } catch (NumberFormatException e){
                Utils.infoBox("Besar Populasi dan Total Plot gunakan integer","Error!");
            }
            System.out.println(totalPlot);
        }
    }

    private class PreviewClicked implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            showPreview();
        }
    }

    private void refreshConfTextArea(){
        LinkedList<String> msgs = GALogic.getHousesTxt();
        confTxtArea.setText("");
        while(msgs.size() > 0){
            String text = msgs.pop();
            if (text.length() > 0){
                confTxtArea.append(text +"\n");
            }
        }
    }

    private class ConfPlotClicked implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent){
            refreshConfTextArea();
        }
    }

    private class AddHouseClicked implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent){
            try{
                int length = Integer.parseInt(lengthTxt.getText());
                int width = Integer.parseInt(widthTxt.getText());
                int price = Integer.parseInt(priceTxt.getText());
                String code = codeTxt.getText();
                if (GALogic.setHouses(code,length,width,price)){
                    refreshConfTextArea();
                } else {
                    Utils.infoBox("Kode telah terdaftar silakan gunakan kode lain", "Error!");
                }

            } catch (NumberFormatException e){
                Utils.infoBox("Gunakan integer untuk panjang, lebar dan harga", "Error!");
            }
        }
    }

    private class DelHouseClicked implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent){
            System.out.println("calling delete");
            String code = delCodeTxt.getText();
            GALogic.delHouse(code);
            refreshConfTextArea();
        }
    }

    private void showPreview(){
        JFrame frame = new JFrame("Land Preview");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        LandPlot land = new LandPlot(landFile.getAbsolutePath(), totalPlot);
        int maxRow = land.getArray().length;
        int maxCol = land.getArray()[0].length;

        landPreview.removeAll();
        landPreview.setLayout(new GridLayout(maxRow, maxCol));
        landPreview.setBackground(Color.white);
        landPreview.setBorder(BorderFactory.createEmptyBorder(1,0,0,0));

        landPreview.setBounds(0,150, maxCol, maxRow);

        JPanel grid[][] = new JPanel[maxRow][maxCol];
        for (int row = 0; row < maxRow; row++){
            for (int col = 0; col < maxCol; col++){
                int temp = land.getArray()[row][col];
                grid[row][col]=new JPanel();

                switch (temp){
                    case LandPlot.GRASS:
                        grid[row][col].setBackground(Color.GREEN);
                        break;
                    case LandPlot.ROAD:
                        grid[row][col].setBackground(Color.GRAY);
                        break;
                    default:
                        grid[row][col].setBackground(Color.white);
                }
                grid[row][col].setBorder(BorderFactory.createLineBorder(new Color(35,35,35)));
                landPreview.add(grid[row][col]);
            }
        }
        landPreview.repaint();
        landPreview.revalidate();

        frame.getContentPane().add(landPreview);
        frame.pack();
        frame.setVisible(true);
    }
}
