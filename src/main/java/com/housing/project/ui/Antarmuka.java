package com.housing.project.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Antarmuka extends JPanel{
	JPanel content;
	JButton bthome,btinput,btproses;

	public Beranda Phome;
	public KelolaData Pinput;
	public OptimalisasiAG Pproses;
	public CardLayout card;
	
	public Antarmuka(){
		this.setLayout(null);
		this.setBackground(Color.WHITE);
		this.setBounds(0, 0, 900,700);
	
		setMenu();
		setcontent();
		setAksi();
	}
	
	public void setMenu(){
		JPanel Menu = new JPanel();
		Menu.setLayout(null);
		Menu.setBackground(Color.BLACK);
		Menu.setBounds(0, 0, 200, 700);
		
		JPanel ptombol = new JPanel();
		ptombol.setLayout(null);
		ptombol.setBackground(Color.DARK_GRAY);
		ptombol.setBounds(0, 200, 200, 500);
		
		bthome = new JButton("Beranda");
		bthome.setBounds(20, 50, 160, 40);
		
		btinput = new JButton("Kelola Data");
		btinput.setBounds(20, 100, 160, 40);
		
		btproses = new JButton("Optimalisasi");
		btproses.setBounds(20, 150, 160, 40);
		btproses.setEnabled(false);

		ptombol.add(bthome);
		ptombol.add(btinput);
		ptombol.add(btproses);

		Menu.add(ptombol);
		this.add(Menu);
	}

	public void setcontent(){
		card = new CardLayout();
		content = new JPanel(card);
		content.setBounds(200, 0, 700, 700);
		
		Phome = new Beranda();
		Pinput = new KelolaData();
		Pproses = new OptimalisasiAG();

		content.add(Phome,"home");
		content.add(Pinput,"input");
		content.add(Pproses,"proses");
		this.add(content);
	}
	
	public void setAksi(){
		
		bthome.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(content, "home");
			}
		});
		
		btinput.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(content, "input");
				btproses.setEnabled(true);
			}
		});
		
		btproses.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(content, "proses");
			}
		});
	}
	
}
