package com.housing.project.ui;

import javax.swing.*;
import java.awt.*;


public class Beranda extends JPanel{
	ImageIcon img = new ImageIcon("asset/home.jpg");
	JLabel labelGambar = new JLabel(img);

	public Beranda(){
		
		labelGambar.setBounds(0,200,700,500);
		this.setLayout(null);
		this.setBackground(Color.LIGHT_GRAY);
		this.add(labelGambar);
		setJudul();
	}
	
	public void setJudul(){
		JPanel paneljudul = new JPanel();
		paneljudul.setLayout(null);
		//judul.setBackground(new Color(17,156,249));
		paneljudul.setBackground(Color.LIGHT_GRAY);
		paneljudul.setBounds(0, 0,700, 200);
		JLabel judul = new JLabel("<html><center>OPTIMALISASI LAHAN TANAH <br>UNTUK AREA RUMAH DAN JALAN <br>MENGGUNAKAN ALGORITMA GENETIKA</center></html>", JLabel.CENTER);
		judul.setBounds(0, 0, 700, 200);
		judul.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 30));
		judul.removeAll();
		this.add(judul);
	}
}
