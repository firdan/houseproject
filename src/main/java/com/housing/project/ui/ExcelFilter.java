package com.housing.project.ui;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by whale on 08/06/16.
 */
public class ExcelFilter extends FileFilter {

    // accept only xls and csv
    public boolean accept(File f){
        if (f.isDirectory()){
            return true;
        }

        String extension = Utils.getExtension(f);
        if (extension != null){
            if (extension.equals(Utils.xls) ||
                    extension.equals(Utils.csv)){
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public String getDescription() { return "Hanya xls / csv yang diperbolehkan ";}
}
