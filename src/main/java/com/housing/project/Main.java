package com.housing.project;

import com.housing.project.ui.Antarmuka;

import javax.swing.*;
import java.awt.*;

public class Main extends JFrame {
    public Antarmuka UI;

    public Main() {
        this.setTitle("Sistem Tugas Akhir");
        this.setLayout(new BorderLayout());
        this.setBackground(Color.darkGray);
        this.setSize(900,700);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        //membuat objek UI dari kelas Antarmuka
        UI = new Antarmuka();
        this.add(UI);

        this.setVisible(true);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        new Main();
    }
}
