package com.housing.project;


public class House {

    public String kode;
    public int length;
    public int width;
    public int price;
    public int id;

    public House(String kode, int length, int width, int price, int id){
        this.kode = kode;
        this.length = length;
        this.width = width;
        this.price = price;
        this.id = id;
    }

    /**
     * get common id for house plot in a land
     * @param id
     * @return
     */
    public static int getCommonId(int id){
        return id/10000;
    }
    public int commonId(){ return id/10000; }
    public String getText() {
        // prevent duplicate
        if (!this.kode.matches(".*-L")) {
            return "kode petak: " + this.kode.toString() + ", (p x l): " + this.length + " x " + this.width + " , harga: " + this.price;
        }
        return "";
    }
}
