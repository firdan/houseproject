package com.housing.project;

import com.housing.project.ui.OptimalisasiAG;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class GALogic {
    private static LandPlot land;
    // define houses
    // 3014
    // 3 -> i=tipe C
    // 0 -> ga landscape
    // 1 -> lahan yang dipake
    // 49 -> sign
    private static House[] houses = {
            new House("A", 12, 8, 186, 10000),
            new House("A-L", 8, 12, 186, 11000),
            new House("B", 12, 6, 144, 20000),
            new House("B-L", 6, 12, 144, 21000),
            new House("C", 10, 6, 102, 30000),
            new House("C-L", 6, 10, 102, 31000)
    };
    private static ArrayList<House> housesList = new ArrayList<House>();
    private Chromosome best;

    private static void printLand(String filename, Chromosome c){
        LandPlot bestLand = new LandPlot(filename, land.getTotalPlot());
        bestLand.setArray(c.getGene());
        bestLand.toExcel();
    }

    public static void setLand(String filepath, int totalPlot){
        land = new LandPlot(filepath, totalPlot);
    }

    public static LinkedList<String> getHousesTxt(){
        LinkedList<String> data= new LinkedList<String>();

        for (int i =0; i< houses.length; i++){
            data.push(houses[i].getText());
        }
        return data;
    }

    public static House[] getHouses(){
        return houses;
    }

    public static boolean setHouses(String code, int length, int width, int price){
        if (housesList.size() < 1){
            housesList = new ArrayList<>(Arrays.asList(houses));
        }

        for (int i =0; i < housesList.size(); i++){
            House house = housesList.get(i);
            if (house.kode.equals(code)) return false;
        }

        int max = housesList.size() / 2;
        // vertical
        housesList.add(new House(code,length,width,price, (max + 1)*10000 ));
        // horizontal
        housesList.add(new House(code+"-L",width,length,price, (max + 1)*10000 + 1000));
        // update house index
        houses = housesList.toArray(new House[housesList.size()]);
        return true;
    }

    public static void delHouse(String code){
        System.out.println("del house");
        if (housesList.size() < 1){
            housesList = new ArrayList<House>(Arrays.asList(houses));
        }
        int length = housesList.size();
        LinkedList<Integer> markIdx = new LinkedList<>();

        // mark index to delete
        // rules of thumb do not delete on search might destroy the length of idx
        for (int i = 0; i < length; i++){
            System.out.println(i +" "+ code + " " + housesList.get(i).kode);
            if (housesList.get(i).kode.matches(code+"(-L)?")){
                markIdx.push(i);
            }
        }

        while (markIdx.size() > 0){
            int temp = markIdx.pop();
            System.out.println(temp);
            housesList.remove(temp);
        }

        // update house index
        houses = housesList.toArray(new House[housesList.size()]);
    }

    public Chromosome getBest (){
        return best;
    }

    public void run(int populationSize, int maxGenerations, double crossoverRatio, double elitsmRatio,
                    double mutationRatio) {

        // create new land
        Chromosome.setLand(land);
        Chromosome.setHouses(houses);

        Population population = new Population(populationSize, houses, land,
                crossoverRatio, elitsmRatio, mutationRatio);


        Chromosome best = population.getPopulations()[0];
        int i=0;
        while (( i++ < maxGenerations )){
            population.evolve();
            best = population.getPopulations()[0];
            printLand("tmp/Generations"+ i +".xls", best);
            OptimalisasiAG.appendLog("Generations "+i+": "+ best.getFitness()+'\n');
            System.out.println("Generations "+i+": "+ best.getFitness());
            OptimalisasiAG.appendLog("kesalahan grid lahan tidak terisi (sisa): "+best.availableLands+"\n");
            OptimalisasiAG.appendLog("raw kesalahan grid lahan tidak terisi (sisa): "+best .rawAvailableLands+"\n");
            OptimalisasiAG.appendLog("kesalahan grid dekat jalan tidak terisi: "+best.nearRoadEmpty+"\n");
            OptimalisasiAG.appendLog("total harga jual: "+best.getPrice()+"\n");
            OptimalisasiAG.appendLog("kesalahan grid sisi terpendek kaveling tidak dekat jalan: "+best.nearHousePlot+"\n");
        }
        this.best = best;

    }
}
