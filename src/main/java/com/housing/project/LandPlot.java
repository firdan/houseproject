package com.housing.project;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class LandPlot {

    private final int totalPlot;

    private String filename;
    private int landArray[][];
    private int availableLand;

    // in which row the road is
    private int rowRoadIdx=-100;
    private int colRoadIdx=-100;

    public static final int GRASS = 0;
    public static final int ROAD = -1;

    public LandPlot(String filename, int totalPlot){

        this.filename = filename;
        this.totalPlot = totalPlot;
    }

    private HSSFWorkbook readFile() throws IOException{
        FileInputStream fis = new FileInputStream(this.filename);
        try {
            return new HSSFWorkbook(fis);
        } finally{
            fis.close();
        }
    }

    //excel to array
    private void toArray (){
        int k = 0;

        // convert to array
        try {
            HSSFWorkbook wb = this.readFile();
            HSSFSheet sheet = wb.getSheetAt(k);
            int rows = sheet.getPhysicalNumberOfRows();
            int cols = sheet.getRow(0).getPhysicalNumberOfCells();

            this.landArray = new int[rows][cols];

            for (int r = 0; r < rows; r++) {
                HSSFRow row = sheet.getRow(r);
                if (row == null) {
                    continue;
                }

                int cells = row.getPhysicalNumberOfCells();

                for (int c = 0; c < cells; c++) {
                    HSSFCell cell = row.getCell(c);
                    //array lahan
                    this.landArray[r][c] = (int) cell.getNumericCellValue();
                }
            }
            wb.close();
        } catch (Exception e ){
            e.printStackTrace();
        }
    }

    private static int[][] copy(int[][] src) {
        int[][] dst = new int[src.length][];
        for (int i = 0; i < src.length; i++) {
            dst[i] = Arrays.copyOf(src[i], src[i].length);
        }
        return dst;
    }

    //Array to excel
    public void toExcel(){
        try{
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet s = wb.createSheet();
            HSSFCellStyle road = wb.createCellStyle();
            HSSFCellStyle grass = wb.createCellStyle();

            HSSFFont f = wb.createFont();
            f.setColor((short) 0xA);
            f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            road.setFont(f);
            road.setFillBackgroundColor(HSSFColor.GREY_40_PERCENT.index);
            road.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
            road.setFillPattern(CellStyle.SOLID_FOREGROUND);

            grass.setFont(f);
            grass.setFillBackgroundColor(HSSFColor.GREEN.index);
            grass.setFillForegroundColor(HSSFColor.GREEN.index);
            grass.setFillPattern(CellStyle.SOLID_FOREGROUND);

            wb.setSheetName(0, "land");


            for (int rownum = 0; rownum < this.landArray.length ; rownum++) {
                HSSFRow r = s.createRow(rownum);

                for (int cellnum = 0; cellnum < this.landArray[rownum].length; cellnum++ ) {
                    HSSFCell c = r.createCell(cellnum);
                    c.setCellValue(this.landArray[rownum][cellnum]);
                    if ( this.landArray[rownum][cellnum] == 0 ) {
                        c.setCellStyle(grass);
                    }else if (this.landArray[rownum][cellnum] == -1){
                        c.setCellStyle(road);
                    }
                    s.setColumnWidth(cellnum, (short) 0x500 );
                }
            }

            FileOutputStream out = new FileOutputStream(this.filename);
            wb.write(out);
            out.close();
            wb.close();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public int[][] getArray() {
        if (this.landArray == null){
            this.toArray();
        }
        return this.landArray;
    }

    /**
     * how many plot / region exist to put houses.
     * @return total plot of exist;
     */
    public int getTotalPlot(){
        return totalPlot;
    }

    public void setArray(int[][] land){
        this.landArray = LandPlot.copy(land);
    }

    public static int calculateLandPlotIdx(int idx){
        return idx % 1000 / 100;
    }

    public static int isLandscape(int idx){
        return idx % 10000/1000;
    }

    public static int assignLandIdx(int houseIdx, int landIdx, int counter){
        landIdx *= 100;
        return houseIdx+landIdx+counter;
    }

    public int getAvailableLand(){
        if (this.availableLand > 0){
            return this.availableLand;
        }

        this.availableLand = 0;
        if (landArray == null){
            getArray();
        }
        for(int row = 0; row < landArray.length; row++){
            for (int  col = 0; col< landArray.length; col++){
                if(landArray[row][col] > 0 && landArray[row][col] <= totalPlot){
                    this.availableLand++;
                }
            }
        }

        return this.availableLand;
    }

    public int getRowRoadIdx (){
        if (rowRoadIdx > -100){
            return rowRoadIdx;
        }
        int [][] landArray = getArray();

        // scan top down looking for road
        for (int col = 0; col< landArray[0].length; col++){
            for (int row = 0; row < landArray.length; row ++){
                if ( landArray[row][col] == -1 ){
                    rowRoadIdx = row;
                    return row;
                }
            }
        }
        return -100;
    }

    public int getColRoadIdx(){
        if (colRoadIdx > -100){
            return colRoadIdx;
        }

        int [][] landArray = getArray();
        for (int col = 0; col < landArray[0].length; col++){
            if (landArray[0][col] == -1){
                colRoadIdx = col;
                return col;
            }
        }
        return -100;
    }
}
