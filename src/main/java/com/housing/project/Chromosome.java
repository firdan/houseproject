package com.housing.project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static java.lang.Math.round;


public class Chromosome implements Comparable<Chromosome>{
    private final int[][] gene;
    private final double fitness;
    private final int[][] filledGene;
    private double price;

    // there should be only one land, no matter how many chromosome.
    private static LandPlot land;
    private static House[] houses;
    // calculate current idx
    private static int counterIdx;


    public int availableLands;
    public int nearRoadEmpty;
    public int nearHousePlot;
    public int rawAvailableLands;

    public Chromosome(int[][] gene){
        this.gene = gene;
        this.fitness = calculateFitness(gene);
        this.price = calculatePrice(gene);
        this.filledGene = processPlot(gene);
    };

    /**
     * menghitung harga dari lahan dengan asumsi 5 plot
     * @param gene
     * @return -1 jika lebih dari 5 plot atau nilai harga dari tanah tersebut
     */
    private int calculatePrice(int[][] gene){
        int price = 0;
        // mencegah error jika total plot lebih dari 5
        if (land.getTotalPlot() > 5){
            return -1;
        }

        final double[] landGrid = new double[] {3, 3.5, 3.5, 2.5, 2.5};
        for (int row = 0; row < gene.length; row++ ){
            for (int col = 0; col< gene[row].length; col++){
                // pastikan lahan terdapat rumah
                // ditambahkan dengan harga tanah
                if (gene[row][col] > 100){
                    int idx = LandPlot.calculateLandPlotIdx(gene[row][col]);
                    price += landGrid[idx-1];
                }
            }
        }
        return price;
    }

    private int[][] processPlot(int [][]gene){
        int [][] filledLand = Chromosome.copy(gene);
        boolean flag = true;
        int empty = 1;

        while ( empty > 0){
            empty = 0;
            for (int row=1; row < filledLand.length-1  ; row++){
                for(int col=1; col < filledLand[row].length-1; col++){
                    int currentPlot = filledLand[row][col];
                    if (currentPlot > 0 && currentPlot <= land.getTotalPlot()){
                        empty ++;
//                        filledLand[row][col] = fillPlot(gene, row, col);
                        filledLand = fillEmptyPlot(filledLand);
                    }
                }
            }
        }

        return filledLand;
    }

    private ArrayList<Integer> candidate;
    private ArrayList<Integer> distances;
    private boolean checkPlot(int currentPlot, int dist){

        if (currentPlot ==LandPlot.GRASS || currentPlot==LandPlot.ROAD){
            return true;
        }
        if (currentPlot > 10){
            candidate.add(currentPlot);
            distances.add(dist);
            return true;
        }
        return false;
    }

    private int fillPlot(int[][] gene, int row, int col){
        candidate = new ArrayList<>();
        distances = new ArrayList<>();
        int dist=0;

        // go down
        for (int i = row; i < gene.length; i++){
            int currentPlot = gene[i][col];
            dist ++;
            if (checkPlot(currentPlot, dist)){
                break;
            }
        }

        // go up
        dist=0;
        for (int i = row; i > 0; i--){
            int currentPlot = gene[i][col];
            dist ++;
            if (checkPlot(currentPlot, dist)){
                break;
            }
        }

        // go right
        dist=0;
        for (int i = col; i < gene[row].length; i++){
            int currentPlot = gene[row][i];
            dist ++;
            if (checkPlot(currentPlot, dist)){
                break;
            }
        }

        // go left
        dist=0;
        for (int i = col; i > 0; i--){
            int currentPlot = gene[row][i];
            dist ++;
            if (checkPlot(currentPlot, dist)){
                break;
            }
        }

        int idxsmall = 0;
        try{
        int temp = distances.get(0);
        for (int i = 1; i < distances.size(); i++) {
            if (temp < distances.get(i)) {
                idxsmall = i;
                temp = distances.get(i);
            }
            return candidate.get(idxsmall);
        }}catch (IndexOutOfBoundsException e){
            System.out.println("t");
        }
        return LandPlot.GRASS;
    }

    private int[][] fillEmptyPlot(int[][]gene){
        int [][] filledLand = Chromosome.copy(gene);

        for (int row=1; row < filledLand.length-1  ; row++){
            for(int col=1; col < filledLand[row].length-1; col++){
                int currentPlot = filledLand[row][col];
                int top = filledLand[row-1][col];
                int left = filledLand[row][col-1];
                int right = filledLand[row][col+1];
                int bottom = filledLand[row+1][col];

                if ( currentPlot > 0 && currentPlot <= land.getTotalPlot() ){
                    // prioritize based on landscape
                    if(LandPlot.isLandscape(currentPlot) == 1){
                        if(top>10){
                            filledLand[row][col]=top;
                        } else if (bottom > 10){
                            filledLand[row][col]=bottom;
                        }
                    } else {
                        if (right>10){
                            filledLand[row][col]=right;
                        }  else if (left>10){
                            filledLand[row][col]=left;
                        }
                    }
                    // clockwise
                    if(currentPlot <= land.getTotalPlot()){
                        if(top>10){
                            filledLand[row][col]=top;
                        } else if (right>10){
                            filledLand[row][col]=right;
                        } else if (bottom > 10){
                            filledLand[row][col]=bottom;
                        } else if (left>10){
                            filledLand[row][col]=left;
                        }
                    }
                }
            }
        }
        return filledLand;
    }

    private double calculateFitness(int[][] gene){
        // things that matter:
        // 1. smaller available land
        // 2. high sell value
        // 3. kavling near road
        // now focus on (1)
        int availableLands = 0;
        int rawAvailableLands=0; // kesalahan dengan
        for (int row = 1; row < gene.length -1; row++){
            for(int col=1; col< gene[row].length -1; col++){
                int top = gene[row-1][col];
                int left = gene[row][col-1];
                int right = gene[row][col+1];
                int bottom = gene[row+1][col];

                if ( gene[row][col] > 0 && gene[row][col] <= land.getTotalPlot()){
                    if(!(top == -1 || left == -1 || right == -1 || bottom == -1)){
                        availableLands ++;
                    }
                    else rawAvailableLands++;
                }
            }
        }

        // menghitung kesalahan lahan dekat jalan apakah lahan kosong atau bukan
        int nearRoadEmpty= 0;

        // hitung kesalahan lahan kosong atau bukan
        for (int row = 1; row < gene.length -1; row++){
            for(int col= 1; col< gene[row].length -1; col++){
                int currentPlot = gene[row][col];
                int top = gene[row-1][col];
                int left = gene[row][col-1];
                int right = gene[row][col+1];
                int bottom = gene[row+1][col];

                // pastikan nilainya bukan rumah dan bukan lahan kosong atau jalan
                if ( currentPlot == -1 && (top <10 && top > 0 || left < 10 && left > 0 || right < 10 && right > 0 || bottom < 10 && bottom > 0) ){
                    nearRoadEmpty++;
                }
            }
        }

        // menghitung kesahalan sisi terpendek tidak dekat dengan jalan
        int nearHousePlot = 0;

        // hitung kesalahan sisi terpanjang menyentuh jalan
        for (int row = 1; row < gene.length -1; row++){
            for(int col= 1; col< gene[row].length -1; col++){
                int currentPlot = gene[row][col];
                int top = gene[row-1][col];
                int left = gene[row][col-1];
                int right = gene[row][col+1];
                int bottom = gene[row+1][col];

                // pastikan nilainya adalah rumah
                if ( currentPlot > 100){
                    int landscape = LandPlot.isLandscape(currentPlot);
                    if ( ((top == -1 || bottom == -1) && landscape == 1) ||
                         ((left == -1 || right == -1) && landscape == 0)){
                        nearHousePlot++;
                    }
                }
            }
        }
        this.nearHousePlot = nearHousePlot;
        this.nearRoadEmpty = nearRoadEmpty;
        this.availableLands = availableLands;
        this.rawAvailableLands = rawAvailableLands;
        return (double) round((double) (availableLands + nearRoadEmpty + nearHousePlot) / land.getAvailableLand() * 1000d) / 1000d;
    }

    public static void setLand(LandPlot landPlot){
        land = landPlot;
    }
    public static void setHouses(House[] houses){
        Chromosome.houses = houses;
    }
    private static int[][] copy(int[][] src) {
        int[][] dst = new int[src.length][];
        for (int i = 0; i < src.length; i++) {
            dst[i] = Arrays.copyOf(src[i], src[i].length);
        }
        return dst;
    }

    /**
     * Logic dalam penempatan petak rumah terhadap lahan, prinsipnya dicoba terlebih dahulu jika gagal array previousLand
     * akan dikembalikan yang berisi nilai petak sebelum adanya perubahan.
     * @param land
     * @param previousLand
     * @param house
     * @param startIdx
     * @param totalPlot
     * @return
     */
    private static int[][] assignHouse(int[][] land, int[][] previousLand, House house, int[] startIdx, int totalPlot){
        // sign to define unique house type
        // each labeled as <houseid><landscape or not><landplot><2digitidx>
        int sign = new Random().nextInt(10);

        for (int row = 0; row < house.length; row++){
            for (int col = 0; col < house.width; col++){
                try {
                    int landRow = row + startIdx[0];
                    int landCol = col + startIdx[1];

                    previousLand[row][col] = land[landRow][landCol];
                    if (land[landRow][landCol] > 0 && land[landRow][landCol] <= totalPlot) {
                        int landIdx = land[landRow][landCol];
                        land[landRow][landCol] = LandPlot.assignLandIdx(house.id,landIdx,counterIdx);
                    } else {
                        return previousLand;
                    }

                } catch( ArrayIndexOutOfBoundsException e){
                    return previousLand;
                }
            }
        }
        return null;
    }

    private static void assignInitialValue(int[][] array, int value){
        for ( int i = 0; i < array.length; i++){
            for (int j = 0; j< array[i].length; j++ ){
                array[i][j] = value;
            }
        }
    }

    /**
     * logic untuk mengubah kondisi menjadi kondisi awal
     * @param land
     * @param revert
     * @param startIdx
     */
    private static void revertToPrevious(int[][] land, int[][] revert, int[] startIdx){

        for (int row = 0; row < revert.length; row++ ){
            for (int col = 0; col < revert[row].length; col++){
                if (revert[row][col] > -100 ){
                    try {
                        int landRow = row + startIdx[0];
                        int landCol = col + startIdx[1];

                        land[ landRow][ landCol] = revert[row][col];
                    }catch (ArrayIndexOutOfBoundsException e){
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    /**
     * menandakan petak rumah untuk ditempatkan di lahan, jika berhasil rumah akan ditempatkan ke lahan
     * jika gagal akan di kembalikan ke kondisi awal
     * @param land array lahan yang akan digunakan
     * @param house tipe rumah yang digunakan
     * @param startIdx index dimana rumah mulai dimasukkan
     * @param totalPlot int total plot lahan
     */
    private static void markLand(int[][] land, House house, int[] startIdx, int totalPlot){

        int[][] previousLand = new int[house.length][house.width];
        assignInitialValue(previousLand, -100);

        int[][] revert = Chromosome.assignHouse(land, previousLand, house, startIdx, totalPlot);

        if (revert != null){
            revertToPrevious(land, revert, startIdx);
        } else {
            counterIdx++;
        }


    }

    static Chromosome generateRandom(House[] houses){
        // reset counterIdx;
        counterIdx=0;

        int[][] dest = Chromosome.copy(land.getArray());

        // assign random housing for plot
        House[] shuffle = new House[land.getTotalPlot()];
        for ( int i=0; i < shuffle.length; i++){
            int idx = new Random().nextInt(houses.length);
            shuffle[i] = houses[idx];
        }

        for (int i=0; i < dest.length; i++ ){
            for (int j=0; j < dest[i].length; j++){
                // make sure it is inside plot index and not marked yet
                if (dest[i][j] > 0 && dest[i][j] <= land.getTotalPlot()){
                    // mark it
                    // get the house index it should be 0 ~ 4 assuming 5 total plot
                    int idx = dest[i][j] - 1;
                    int[] startIdx = new int[] {i,j};
                    Chromosome.markLand(dest, shuffle[idx], startIdx, land.getTotalPlot());
                }
            }

        }

        return new Chromosome(dest);
    }

    /**
     * @return gene for this chromosome
     */
    public int[][] getGene(){
        return gene;
    }

    public int[][] getFilledGene(){
        return filledGene;
    }
    /**
     * @return fitness for this chromosome, smaller is better
     */
    public double getFitness(){return fitness; }

    /**
     * @return total land price for this chromosome.
     */
    public double getPrice(){ return price; }

    //Gene
    private List<Integer> scanHouseId(int[][] newGene){
        List<Integer> houses= new ArrayList<Integer>();
        int housesSize = 0;
        for (int row = 0; row < newGene.length; row++){
            for (int col = 0; col < newGene[row].length; col++){
                boolean skip = false;
                for (int idx = 0; idx < housesSize; idx++ ){
                    if (houses.get(idx) == newGene[row][col]){
                        skip =true;
                        break;
                    }
                }
                if (!skip && newGene[row][col] > land.getTotalPlot()){
                    houses.add(newGene[row][col]);
                    housesSize = houses.size();
                }
            }
        }
        return houses;
    }

    public Chromosome mutate(){
        counterIdx = 0;
        int[][] newGene = Chromosome.copy(gene);

        // pilih salah satu plot untuk di random
        int landPlotIdx = new Random().nextInt(land.getTotalPlot()) + 1;

        // clear the land
        for (int row = 0; row < newGene.length; row++){
            for (int col = 0; col< newGene[row].length; col++){
                int currentLandPlotIdx;
                if (newGene[row][col] < 10){
                    currentLandPlotIdx = newGene[row][col];
                } else{
                    currentLandPlotIdx = LandPlot.calculateLandPlotIdx(newGene[row][col]);
                }
                if (currentLandPlotIdx == landPlotIdx){
                    newGene[row][col] = landPlotIdx;
                }
            }
        }

        // choose random house
        House house = Chromosome.houses[new Random().nextInt(Chromosome.houses.length)];

        // assign new random house to cleaned plot
        for (int i=0; i < newGene.length; i++ ){
            for (int j=0; j < newGene[i].length; j++){
                // make sure it is inside plot index and not marked yet
                if (newGene[i][j] == landPlotIdx){
                    int[] startIdx = new int[] {i,j};
                    Chromosome.markLand(newGene, house, startIdx, land.getTotalPlot());
                }
            }

        }

        return new Chromosome(newGene);
    }

    /**
     * for debug purposes to print gene (land)
     * @param filename
     * @param testLand gene to print
     */
    private static void printLand(String filename, int[][] testLand){
        LandPlot bestLand = new LandPlot(filename, land.getTotalPlot());
        bestLand.setArray(testLand);
        bestLand.toExcel();
    }

    public Chromosome[] mate(Chromosome mate){
        int[] temp;
        // married them
        int [][] parent1 = gene;
        int [][] parent2 = mate.gene;

        int [][] child1 = new int[gene.length][];
        int [][] child2 = new int[gene.length][];

        child1= Chromosome.copy(parent1);
        child2= Chromosome.copy(parent2);

        // perkawinan pada salah satu blok
        // pilih secara random blok yang akan di "kawinkan"
        int landPlotIdx = new Random().nextInt(land.getTotalPlot()) + 1;

        // lakukan pertukaran
        for (int row = 0; row < gene.length; row++){
            for (int col=0; col < gene[row].length; col++){

                // ambil index lahan dan lakukan pertukaran
                int currentLandPlotIdx;
                if (gene[row][col] < 10){
                    currentLandPlotIdx = gene[row][col];
                } else {
                    currentLandPlotIdx = LandPlot.calculateLandPlotIdx(gene[row][col]);
                }

                if ( currentLandPlotIdx == landPlotIdx ){
                    child1[row][col] = parent2[row][col];
                    child2[row][col] = parent1[row][col];
                }
            }
        }

        return new Chromosome[]{ new Chromosome(child1), new Chromosome(child2)};
    }

    @Override
    public int compareTo(Chromosome c){
        if (c == null){
            return -1;
        }
        if (fitness < c.fitness){
            return -1;
        } else if (fitness > c.fitness ){
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o){
        if( !(o instanceof Chromosome)){
            return false;
        }

        Chromosome c = (Chromosome) o;
        for (int i = 0; i < gene.length; i++){
            if(Arrays.equals(gene[i], c.gene[i])){
                continue;
            } else{
                return false;
            }
        }

        return fitness == c.fitness;
    }
}
