package com.housing.project;

import java.util.*;


public class Population {

    private static final Random rand = new Random(System.currentTimeMillis());

    private double elitism;
    private double mutation;
    private double crossover;
    private Chromosome[] populations;

    // menyimpan informasi rumah yang akan dibangun
    private House[] houses;

    public Population(int size, House[] houses,LandPlot land, double crossoverRatio, double elitismRatio,
                      double mutationRatio){
        this.crossover = crossoverRatio;
        this.elitism = elitismRatio;
        this.mutation = mutationRatio;

        this.houses = houses;

        this.populations = new Chromosome[size];
        for (int i = 0; i< size; i++){
            this.populations[i] = Chromosome.generateRandom(houses); //random kromosom populasi awal
        }
        Arrays.sort(populations);
    }

    public Chromosome[] getPopulations(){
        Chromosome[] arr = new Chromosome[populations.length];
        System.arraycopy(populations, 0, arr, 0, populations.length);

        // sort from the smallest fitness
        Arrays.sort(arr);
        return arr;
    }

    public void evolve(){
        //  buffer for new generations
        Chromosome[] buffer = new Chromosome[populations.length];
        System.arraycopy(populations, 0, buffer, 0, populations.length);

        // do we need ?
        int idx = 1;

        while ( idx < buffer.length){
            if (rand.nextFloat() <= crossover){

                // crossover / "mating"
                Chromosome[] parents = selectParents();
                Chromosome[] children = parents[0].mate(parents[1]);

                // first child mutation
                if ( rand.nextFloat() < mutation ){
                    buffer[idx] = children[0].mutate();
                } else {
                    buffer[idx] = children[0];
                }

                // second child mutation if possible
                if (idx < buffer.length) {
                    if ( rand.nextFloat() < mutation) {
                        buffer[idx] = children[1].mutate();
                    } else {
                        buffer[idx] = children[1];
                    }
                }
            } else {
                if (rand.nextFloat() <= mutation ){
                    buffer[idx] = populations[idx].mutate();
                } else {
                    buffer[idx] = populations[idx];
                }
            }
            idx ++;
        }

        Arrays.sort(buffer); //sorting pelanggaran

         //reset populations
         populations = buffer;

    }

    private Chromosome rouletteSelect(int total_fitness){
        // from wiki fitnes propotionate selection

        // 0.0 ~ 1.0
        double value = rand.nextDouble() * total_fitness;
        for ( int col = 0; col < populations.length; col++){
            value -= populations[col].getFitness();
            if ( value <= 0){
                return populations[col];
            }
        }
        return populations[populations.length - 1 ];
    }

    private Chromosome[] selectRandom(){
        // method
        // selection algorithm using roulette
        Chromosome[] parents = new Chromosome[2];
        int total_fitness = 0;


        for ( int col = 0; col < populations.length; col++ ){
            total_fitness += populations[col].getFitness();
        }

        parents[0] = rouletteSelect(total_fitness/2);
        parents[1] = rouletteSelect(total_fitness/2);
        return parents;
    }

    // milih induk
    private Chromosome[] selectParents(){
        //return selectRandom();
        Chromosome[] parents = new Chromosome[2];

        do {
            parents[0] = populations[new Random().nextInt(populations.length / 2)];
            parents[1] = populations[new Random().nextInt(populations.length / 2)];
        } while (parents[0] == parents[1]);
        return parents;
    }
}
